package Exo2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;

import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;

public class XMLUtil {

	//methode serialize
	public Document Serialize(Marin m){
		// creation document
		Document document = DocumentHelper.createDocument();
		// creation Element a la racine , attribut id : 12
		Element root = document.addElement("marin").addAttribute("id",m.getId());
		// sous element nom
		Element nom = root.addElement("nom");
		// nom de cet element
		nom.addText(m.getNom());
		// sous element prenom
		Element prenom = root.addElement("prenom");
		// ajout de texte
		prenom.addText(m.getPrenom());
		Element age = root.addElement("age");
		age.addText(m.getAge());

		return document;
	}

	//methode write 
	public void write(Document document , File file) 
			throws Exception{
		OutputStream out= new FileOutputStream(file);
		OutputFormat format= OutputFormat.createPrettyPrint();
		XMLWriter writer = new XMLWriter(out,format);
		writer.write(document);
		writer.flush();
	}

	//methode read
	public Document read(File file) throws DocumentException {
		SAXReader reader = new SAXReader();
		Document document = reader.read(file);
		return document;
	}

	//methode deserializable
	public Marin deserialize(Document document){
		
		Element root =document.getRootElement();
		
		//root attributes
		List listAttribut =root.attributes();
		
		
		//first attribute
		Attribute attr=(Attribute) listAttribut.get(0);
		
		//value
		String idString =attr.getValue();
		long id= Long.parseLong(idString);
		
		//List of elemnts 
		List listElements= root.elements();
		Element e1= (Element)listElements.get(0);
		Element e2= (Element)listElements.get(1);
		Element e3= (Element)listElements.get(2);
		String nom = e1.getText();
		String prenom =e2.getText();
		int age=Integer.parseInt(e3.getText());
		
		//instanciation objet marin
		Marin m = new Marin();
		m.setId(id);
		m.setNom(nom);
		m.setPrenom(prenom);
		m.setAge(age);
		return m;
	}


}
