package Exo1;

public class Marin {


	private long id;
	private String nom;
	private String prenom;
	private int age;
	
	public Marin(){}
	
	//Id getter-setter
	public String getId(){
		return Long.toString(this.id);
	}
	public void setId(long Id){
		this.id=Id;
	}
	
	//Name getter-setter
	public String getNom(){
		return this.nom;
	}
	public void setNom(String nom){
		this.nom=nom;
	}
	
	//Prenom getter-setter
	public String getPrenom(){
		return this.prenom;
	}
	public void setPrenom(String prenom){
		this.prenom=prenom;
	}
	
	//Age getter-setter
	public String getAge(){
		return Integer.toString(this.age);
	}
	public void setAge(int age){
		this.age=age;
	}
	
	
	
		
}

