package Exo1;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;

public class XMLUtil {

	//methode serialize
	public Document Serialize(Marin m){
		// creation document
		Document document = DocumentHelper.createDocument();
		// creation Element a la racine , attribut id : 12
		Element root = document.addElement("marin").addAttribute("id",m.getId());
		// sous element nom
		Element nom = root.addElement("nom");
		// nom de cet element
		nom.addText(m.getNom());
		// sous element prenom
		Element prenom = root.addElement("prenom");
		// ajout de texte
		prenom.addText(m.getPrenom());
		Element age = root.addElement("age");
		age.addText(m.getAge());
		
		return document;
	}

	//methode write 
	
	public void write(Document doc , File file) 
	throws Exception{
		OutputStream out= new FileOutputStream(file);
		OutputFormat format= OutputFormat.createPrettyPrint();
		XMLWriter writer = new XMLWriter(out,format);
		writer.write(doc);
		writer.flush();
	}
		
	

}
