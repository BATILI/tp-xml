package Exo3;

import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

public class HandlerCount extends DefaultHandler{
	int cpt=0;
	public void startDocument(){
		System.out.println("Debut de la lecture");		
	}
	
	public void startElement (String uri,String localName,String qname,Attributes attributes){
		cpt++;	
	}
	
	
	public void endDocument() {
		System.out.println("Nombre de sous-elements de l'element racine : " + (cpt-1));
		System.out.println("Fin de la lecture\n");
	}

}
