package Exo3;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class HandlerCategories extends DefaultHandler {
	boolean isCategory=false;
	List<String> list = new ArrayList<String>();
	public void startDocument(){
		System.out.println("Debut de la lecture");		
	}
	
	public void startElement (String uri,String localName,String qname,Attributes attributes){
		if(qname.equals("category"))
			isCategory=true;
		
	}
	
	public void characters(char[] ch, int start, int length) throws SAXException {
		String donnees= new String(ch,start,length);
		if(isCategory==true && donnees.contains("Cloud"))
			list.add(donnees);
	}
	
	public void endElement(String uri, String localName, String qName) throws SAXException {
		isCategory=false;
	}
	
	public void endDocument() {
		System.out.println("Categories contenant Cloud");
		for (String s : list)
			System.out.println(s);
		System.out.println("Fin de la lecture\n");
	}

}
