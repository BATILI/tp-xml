package Exo3;

import java.io.IOException;
import java.io.InputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.http.client.ClientProtocolException;

public class Main {
	public static void main(String [] args) throws ClientProtocolException, IOException{
		String url = "https://www.voxxed.com/feed/";
		InputStream inputsream,inputsream1,inputsream2,inputsream3,inputsream4;
		RSSReader rssreader= new RSSReader();
		inputsream= rssreader.read(url);
		inputsream1 = rssreader.read(url);
		inputsream2 = rssreader.read(url);
		inputsream3 = rssreader.read(url);
		inputsream4 = rssreader.read(url);
		try{
			SAXParserFactory f =SAXParserFactory.newInstance();
			f.setValidating(true);
			f.setNamespaceAware(true);
			SAXParser saxparse= f.newSAXParser();
			saxparse.parse(inputsream, new HandlerNom());
			saxparse.parse(inputsream1, new HandlerCount());
			saxparse.parse(inputsream2, new HandlerItems());
			saxparse.parse(inputsream3, new HandlerTitleItems());
			saxparse.parse(inputsream4, new HandlerCategories());
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

 
}
	

