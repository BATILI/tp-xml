package Exo3;

import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

public class RSSReader {

	public InputStream read(String url) throws ClientProtocolException, IOException{
		
		//ouverture d'un client 
		HttpClient httpClient = HttpClientBuilder.create().build();
		
		//creation d'une methode http
		HttpGet get = new HttpGet(url);
		
		HttpResponse response =httpClient.execute(get);
		StatusLine statusLine= response.getStatusLine();
		
		//invocation de la m�thode 
		int statusCode =statusLine.getStatusCode();
		if(statusCode!= HttpStatus.SC_OK){
			System.err.println("Method failed: "+statusLine);
		}
		
		//lecture de la reponse dans un flux
		InputStream resp=  response.getEntity().getContent();
		return resp;
	}
	
}
